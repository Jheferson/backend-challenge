<?php
use Goutte\Client;
/**
 * BuscarMarcas
 * @author Jheferson Fonseca Gonçalves <play_jheferson@hotmail.com>
 */
class BuscarMarcas
{
    /**
     * Metodo responsável por extrair as marcas utilizadas no site.
     *
     * @return array
     */
    public static function getMarcas()
    {
        $url = UrlSemiNovosBh::URL_MARCAS_MODELOS;

        $marcas = TRequest::sendRequest($url,'GET')[1]['marcas'];
        $ret = [];
        foreach ($marcas as $key => $marca) {
            unset($marca['modelos']);
            $ret[] = $marca;
        }
        return array_values($ret);
    }
}
