<?php
use Goutte\Client;
/**
 * BuscarDetalhesVeiculo
 * @author Jheferson Fonseca Gonçalves <play_jheferson@hotmail.com>
 */
class BuscarDetalhesVeiculo
{
    /**
     * Método responsável por buscar os detalhes do veiculo no site
     *
     * @param array $params
     * @return array
     */
    public static function getVeiculo($params)
    {
        try {
            
            $url = UrlSemiNovosBh::URL_DETALHES_VEICULO;
            $url = sprintf($url, $params["id"]);

            TRequest::get();

            $veiculo = [];

            $client = new Client();
            $crawler = $client->request('GET', $url);

            $veiculo['detalhes'] = $crawler->filter('div#infDetalhes > span')->each(function($node){
                return $node->filterXPath("//li")->extract(['_text']);
            })[0];
           
            $veiculo['acessorios'] = $crawler->filter('div#infDetalhes2')->each(function($node){
                return $node->filterXPath("//li")->extract(['_text']);
            })[0];

            $veiculo['observacoes'] = $crawler->filter('div#infDetalhes3')->each(function($node){
                return $node->filterXPath("//p")->extract(['_text']);
            })[0];

            $veiculo['contato'] = $crawler->filter('div#infDetalhes4')->each(function($node){
                return $node->filterXPath("//li")->extract(['_text']);
            })[0];

            $veiculo['fotos'] = $crawler->filter('div#conteudoVeiculo')->each(function($node){
                return $node->filterXPath("//img[@class='img_borda thumb-veiculo']")->extract(['src']);
            })[0];

            return $veiculo;
            
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

}
