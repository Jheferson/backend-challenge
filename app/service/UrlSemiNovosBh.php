<?php

class UrlSemiNovosBh
{
    const URL_SEMINOVOSBH_TODOS = "https://seminovos.com.br/carro/%s/%s/cidade[]%s?ordenarPor=2&registrosPagina=1000";
    const URL_SEMINOVOSBH_BUSCA_AVANCADA = "https://seminovosbh.com.br/buscaavancada/";
    const URL_MARCAS_MODELOS = "https://seminovos.com.br/filtros?1.5.11";
    const URL_DETALHES_VEICULO = "https://www.seminovosbh.com.br/comprar////%s";
}
