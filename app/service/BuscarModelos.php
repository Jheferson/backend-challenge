<?php
use Goutte\Client;
/**
 * BuscarModelos
 * @author Jheferson Fonseca Gonçalves <play_jheferson@hotmail.com>
 */
class BuscarModelos
{
    /**
     * Método responsável por buscar os modelos de uma marca específica
     *
     * @param array $params
     * @return void
     */
    public static function getModelos($params)
    {
        
        $url = UrlSemiNovosBh::URL_MARCAS_MODELOS;

        $modelos = TRequest::sendRequest($url,'GET')[1]['marcas'];
        $ret = [];
        foreach ($modelos as $key => $modelo) {
            
            if($params['marca'] == $modelo['id']){
                $ret = $modelo['modelos'];
            }
        }
        return $ret;
    }
}
