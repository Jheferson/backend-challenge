<?php
use Goutte\Client;

class BuscarCidades
{

    public static function getCidades()
    {
        $url = UrlSemiNovosBh::URL_SEMINOVOSBH_BUSCA_AVANCADA;

        $client = new Client();
        $crawler = $client->request('GET', $url);
        $cidades = $crawler->filter('#idCidade > option')->each(function ($node) {

            $cidade = new stdClass;
            $cidade->id = $node->extract(['value'])[0];
            $cidade->nome = $node->text();

            return $cidade;
            
        });

        unset($cidades[0]);

        return array_values($cidades);
    }
}
