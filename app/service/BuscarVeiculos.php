<?php
use Goutte\Client;

/**
 * BuscarVeiculos
 * @author Jheferson Fonseca Gonçalves <play_jheferson@hotmail.com>
 */
class BuscarVeiculos
{
    /**
     * Método responsável por buscar veiculos no site e lista-los
     *
     * @param array $params
     * @return array
     */
    public static function getVeiculos($params)
    {
        try {
            
            $url = UrlSemiNovosBh::URL_SEMINOVOSBH_TODOS;
            $url = sprintf($url, $params["marca"], $params["modelo"], $params["cidade"]);

            if(empty($params["marca"]) || empty($params["modelo"])){
                throw new Exception("Informe uma marca e um modelo!");
            }
            $client = new Client();
            $crawler = $client->request('GET', $url);
            $veiculos = $crawler->filter('.card-nitro-home')->each(function ($node) {

                $veiculo = new stdClass;
                $veiculo->id = $node->filterXPath("//meta[@itemprop = 'productID' ]")->extract(['content'])[0];
                $veiculo->nome = $node->filterXPath("//span[@itemprop = 'name' ]")->extract(['_text'])[0];
                $veiculo->modelo = $node->filterXPath("//span[@itemprop = 'model' ]")->extract(['_text'])[0];
                $veiculo->marca = $node->filterXPath("//span[@itemprop = 'brand' ]")->extract(['_text'])[0];
                $veiculo->descricao = $node->filterXPath("//span[@itemprop = 'description' ]")->extract(['_text'])[0];
                $veiculo->kmrodado = (float) $node->filterXPath("//span[@itemprop = 'mileageFromOdometer' ]")->extract(['_text'])[0];
                $veiculo->valor = (float) $node->filterXPath("//span[@itemprop = 'price' ]")->extract(['_text'])[0];
                $veiculo->ano = (float) $node->filterXPath("//li[@title = 'Ano de fabricação' ]")->extract(['_text'])[0];
                $veiculo->link = $node->filterXPath("//meta[@itemprop = 'url' ]")->extract(['content'])[0];
                //Tratativa especial, para correção de bug causado por um delay do AngularJS
                $veiculo->capa = $node->filterXPath("//img[@itemprop='image']")->extract(['data-src'])[0] == '' ? $node->filterXPath("//img[@itemprop='image']")->extract(['src'])[0] : $node->filterXPath("//img[@itemprop='image']")->extract(['data-src'])[0];
                
                return $veiculo;

            });

            return array_values($veiculos);
            
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

}
