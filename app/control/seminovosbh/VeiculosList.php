<?php
/**
 * VeiculosList
 * @author Jheferson Fonseca Gonçalves <play_jheferson@hotmail.com>
 */
class VeiculosList extends TPage
{
    private $form;
    private $datagrid;
    private $pageNavigation;
    private $loaded;
    private static $formName = 'formList_Veiculos';
    private $marcas = [];

    /**
     * Class constructor
     * Cria um formulário com um grid
     */
    public function __construct()
    {
        parent::__construct();
        $this->form = new BootstrapFormBuilder(self::$formName);

        $this->form->setFormTitle('Listagem de Veiculos');

        $cidade = new TCombo('cidade');
        $marca  = new TCombo('marca');
        $modelo  = new TCombo('modelo');

        $cidade->addItems($this->getCidades());
        $marca->addItems($this->getMarcas());

        $this->onChangeMarca(['key'=> $marca->getValue()]);

        $change_action = new TAction(array($this, 'onChangeMarca'));
        $marca->setChangeAction($change_action);

        $cidade->setSize('100%');
        $marca->setSize('100%');

        $row1 = $this->form->addFields([new TLabel('Cidade:', null, '14px', null)],[$cidade]);
        $row2 = $this->form->addFields([new TLabel('Marca:', null, '14px', null)],[$marca],[new TLabel('Modelo', null, '14px', null)],[$modelo]);

        $this->form->setData( TSession::getValue(__CLASS__.'_filter_data') );

        $btn_onsearch = $this->form->addAction('Buscar', new TAction([$this, 'onSearch']), 'fa:search #ffffff');
        $btn_onsearch->addStyleClass('btn-primary'); 

        $this->datagrid = new TDataGrid;
        $this->datagrid = new BootstrapDatagridWrapper($this->datagrid);

        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);

        $column_capa = new TDataGridColumn('capa', 'Capa', 'left' , '70px');
        $column_nome = new TDataGridColumn('nome', 'Nome', 'left');
        $column_descricao = new TDataGridColumn('descricao', 'Descricao', 'left');
        
        $column_capa->setTransformer(array($this, 'formatCapa'));
        
        $column_estado_nome = new TDataGridColumn('estado->nome', 'Estado', 'left');

        $this->datagrid->addColumn($column_capa);
        $this->datagrid->addColumn($column_nome);
        $this->datagrid->addColumn($column_descricao);
        

        $action1 = new TDataGridAction(array($this, 'verDetalhes'));
        $action1->setLabel('Ver Detalhes');
        $action1->setImage('fa:eye #7C93CF');
        $action1->setField('link');

        $action_group = new TDataGridActionGroup('Actions ', 'bs:th');
        
        $action_group->addAction($action1);
        
        $this->datagrid->addActionGroup($action_group);
        
        $this->datagrid->createModel();

        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());

        $panel = new TPanelGroup;
        $panel->add($this->datagrid);

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($panel);

        parent::add($container);
    }

    /**
     * Método responsável por buscar o modelos apos selecionar uma marca
     *
     * @param array $params
     * @return array
     */
    public static function onChangeMarca($params){
        $url = 'http://'. $_SERVER['HTTP_HOST'] .'/backend-challenge/modelos/'. $params['key'];
        $modelos = TRequest::sendRequest( $url,'GET');
        $ret = [];
        
        foreach ($modelos[1] as $key => $modelo) {
            $ret[$modelo["nome"]] = $modelo["nome"];
        }
        
        TCombo::reload('formList_Veiculos', 'modelo', $ret);
    }

    /**
     * Método responsável por abrir pagina detalhada sobre o produto no site
     *
     * @param array $params
     * @return void
     */
    public function verDetalhes($params){

        $window = TWindow::create("Detalhes", 0.8, 650);
        
        $iframe = new TElement('iframe');
        $iframe->id = "iframe_external";
        $iframe->src = $params["key"];
        $iframe->frameborder = "0";
        $iframe->scrolling = "yes";
        $iframe->width = "100%";
        $iframe->height = "600px";
        
        $window->add($iframe);
        $window->show();

    }

    /**
     * Método responsável por acessar a api RESTFULL que lista as cidades
     *
     * @return array
     */
    private function getCidades(){
        $cidades = TRequest::sendRequest('http://'. $_SERVER['HTTP_HOST'] .'/backend-challenge/cidades','GET');
        $ret = [];

        foreach ($cidades[1] as $key => $cidade) {
            $ret[$cidade['id']] = $cidade["nome"];
        }
        
        return $ret;
    }

    /**
     * Método responsável por acessar a api RESTFULL que lista as marcas
     *
     * @return array
     */
    private function getMarcas(){
        $marcas = TRequest::sendRequest('http://'. $_SERVER['HTTP_HOST'] .'/backend-challenge/marcas','GET');
        $ret = [];

        foreach ($marcas[1] as $key => $marca) {
            $ret[$marca['id']] = $marca["nome"];
        }
        $this->marcas = $ret;
        return $ret;
    }

    /**
     * Register the filter in the session
     */
    public function onSearch()
    {
        // get the search form data
        $data = $this->form->getData();
        $filters = [];

        TSession::setValue(__CLASS__.'_filter_data', NULL);
        TSession::setValue(__CLASS__.'_filters', NULL);

        $param = array();

        // keep the search data in the session
        
        TSession::setValue(__CLASS__.'_filter_data', $data);
        TSession::setValue(__CLASS__.'_filters', $filters);

        self::onChangeMarca(['key'=>$data->marca]);
        
        $this->form->setData($data);

        $this->onReload($data);
    }

    /**
     * Load the datagrid with data
     */
    public function onReload($param = NULL)
    {
        try
        {
            $param = (array)$param;
            $this->datagrid->clear();
            $url = 'http://'. $_SERVER['HTTP_HOST'] .'/backend-challenge/veiculos/'.$this->marcas[$param['marca']].'/'.$param['modelo'].'/'.$param['cidade'];
            
            $objects = TRequest::sendRequest($url,'GET')[1];
            
            if ($objects)
            {
                // iterate the collection of active records
                foreach ($objects as $object)
                {
                    $this->datagrid->addItem((object)$object);
                }
            }
            $this->loaded = true;
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new TMessage('error', $e->getMessage());
            // undo all pending operations
            TTransaction::rollback();
        }
    }

    public function formatCapa($capa, $object, $row)
    {
        return "<img src='{$capa}' style='width:150px; height: 100px' />";
    }

    public function onShow($param = null)
    {

    }

    /**
     * method show()
     * Shows the page
     */
    public function show()
    {
        parent::show();
    }

}

