## O Desafio 
Este repositório é uma solução para o desafio proposto [aqui](https://bitbucket.org/alpesoneprojetosespeciais/backend-challenge/src/master/ "aqui")
Onde foi solicitado o desenvolvimento de uma API RESTFULL para extrair alguns dados do site SeminovosBH.
O desafio pediu que fosse criado dois endpoints, onde um seria responsável por buscar veiculos no site por meio de filtros e outro para listar os detalhes de um veiculo específico.
Segue abaixo os detalhes da solução aplicada.


## A solução
Além dos dois endpoints solicitados, criei mais outros 3 para que fosse possível criar uma solução mais completa, com exemplos de consumo das APIs.

#### Tecnologias utilizadas
- **Adianti Framework**: é uma arquitetura open-source para criação de sistemas em PHP, focado no desenvolvimento de aplicações de negócio. Para acelerar a criação de sistemas, ele oferece uma série de componentes para a construção de formulários e datagrids, e recursos para relatórios, gráficos, etiquetas, calendários, e outros. Possui templates prontos e funcionais com controle de permissões, logs, interação entre usuários, etc. Dessa forma, o programador pode focar na lógica e na regra de negócios, não em detalhes da implementação.
- **Goutte Lib**: Goutte é uma biblioteca de rastreio de tela e rastreamento da Web para PHP. Goutte fornece uma ótima API para rastrear sites e extrair dados das respostas HTML / XML.

## Instalação
Clone este repositorio no raiz do seu servidor web e execute o comando **composer update** para a instalação das dependencias.
Para acessar a aplicação voce deve acessar: http://seudominio/backend-challenge/ este será o diretório raiz da aplicação.

## Endpoints
Os endpoints abaixo são os endpoints principais, requeridos no teste:

- **/backend-challenge/veiculos/"marca"/"modelo"/"cidade"**: Este endpoint é responsável por fazer uma busca de veiculos no site.Veja o exemplo em funcionamento [aqui](http://duirbyoak.com.br/backend-challenge/veiculos/BMW/1301/ "aqui")

- **/backend-challenge/veiculo/"id_veiculo"**: Este endpoint é responsável por extrair os detalhes do veículo no site.Veja o exemplo em funcionamento [aqui](http://duirbyoak.com.br/backend-challenge/veiculo/2629721 "aqui")

O endpoints abaixo forão criados para serem consumidos, pela aplicação demonstrativa:

- **/backend-challenge/cidades**: Este endpoint é responsável por listar as cidades utilizadas nos filtros do site.Veja o exemplo em funcionamento [aqui](http://duirbyoak.com.br/backend-challenge/cidades "aqui")

- **/backend-challenge/marcas**: Este endpoint é responsável por listar as marcas utilizadas nos filtros do site.Veja o exemplo em funcionamento [aqui](http://duirbyoak.com.br/backend-challenge/marcas "aqui")

- **/backend-challenge/modelos/"id_marca"**: Este endpoint é responsável por listar os modelos utilizados nos filtros do site.Veja o exemplo em funcionamento [aqui](http://duirbyoak.com.br/backend-challenge/marcas "aqui")

## App Demonstrativo

![Print](https://i.ibb.co/2jP45gT/Busca-de-Veiculos.png "Print")

Para demonstrar de maneira visual, criei uma aplicação simples que faz uso dos endpoints listados anteriormente, para buscar veiculos no site.
Utilizei o andianti para criar tudo na mesma estrutura de arquivos.
Acesse localmente com http://localhost/backend-challenge/ ou acesse o link abaixo para acessar o sistema online:

[backend-challenge](http://duirbyoak.com.br/backend-challenge/ "aqui")

**Usuário:** user

**Senha:** user

## Considerações
A cada novo desafio eu aprendo algo novo, e dessa vez eu conheci algumas ferramentas bem interessantes como Goutte Lib, realmente fiquei impressionado com ela.
Durante as pesquisas acabei encontrado algumas falhas de segurança no site do seminovosbh, que deixa o site vulnerável a ataques de negação DDOS.
Fiquei muito contente com o resultado obtido, sempre me sinto motivado com desafios desse tipo, agradeço a oportunidade, muito obrigado.